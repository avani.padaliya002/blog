<?php
session_start();
include('config.php');
if (isset($_SESSION['id']) && isset($_SESSION['username'])) {

    $query = "select * from blogs";
    $exeQuery = mysqli_query($conn, $query);
    if (mysqli_num_rows($exeQuery) >= 1) {
        $blogs = mysqli_fetch_all($exeQuery, MYSQLI_ASSOC);
    } else {
        $noblog = "No blog found :(";
    }
    ?>
    <html>
    <head>
        <title>Blogs</title>
        <link rel="stylesheet" type="text/css" href="style.css">

    </head>
    <body>
    <div class="header">
        <div class="menu-bar">
            <nav>
                <ul class="menu">
                    <li class=''><a href="home.php">Home</a></li>
                    <li class=''><a href="blogs.php">Blogs</a></li>
                    <li class=''><a href="logout.php">Logout</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="main">
        <div style="height: 100px; ">
            <a href="createBlog.php">
                <button type="submit" class="create">Create Blog</button>
            </a>
        </div>
        <?php if (isset($_SESSION['error'])) { ?>
            <div class="alert">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                <?php echo $_SESSION['error'];
                unset($_SESSION['error']); ?>

            </div>
        <?php } ?>

        <?php if (isset($_SESSION['success'])) { ?>
            <div class="alert success">
                <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                <?php echo $_SESSION['success'];
                unset($_SESSION['success']); ?>
            </div>
        <?php } ?>

        <div class="blog-grid">
            <?php if (isset($noblog)) { ?> <p style="text-align: center"><?= $noblog ?></p> <?php } ?>
            <?php
            foreach ($blogs as $blog) { ?>
                <div style="text-align:justify; box-shadow: 0 0 20px 16px #0000001f; padding: 10px;margin: 10px;">
                    <img src="/blog/images/<?= $blog['image'] ?>" style="height: 250px; width:415px;">
                    <h4>Title:</h4><?= $blog['title']; ?>
                    <h4>
                        Description:</h4><?php echo substr($blog['description'], 0, 100) . ((strlen($blog['description']) > 100) ? '...' : ''); ?>
                    <h4>Date:</h4><?php $date = date_create($blog['date']);
                    echo date_format($date, 'Y-M-d'); ?>
                    <button onclick="deleteBlog(<?= $blog['id'] ?>)">Delete</button>
                    <a href="createBlog.php?id=<?= $blog['id'] ?> ">
                        <button>Edit</button>
                    </a>
                    <a href="view.php?id=<?= $blog['id'] ?> ">
                        <button>View</button>
                    </a>
                </div>
            <?php } ?>

        </div>
    </div>
    <div class="footer">
        <div class="menu-bar">
            <nav>
                <footer class="footer">
                    All right reserved
                </footer>
            </nav>
        </div>
    </div>
    </body>
    </html>

    <?php
} else {
    header("Location:login.php");
    exit();
} ?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">

    function deleteBlog(data) {
        if (confirm("Are you sure you want to delete Blog?")) {
            $.ajax({
                type: "POST",
                url: "deleteBlog.php",
                data: {"id": data},
                dataType: "json",
                success: function (data) {
                    window.location.href = "blogs.php";
                },
                error: function (request, error) {
                    alert("Request: " + JSON.stringify(request));
                }
            });
        }
    }

    $("document").ready(function () {
        $(function () {
            $('.menu a[href="' + location.pathname.split("/")[location.pathname.split("/").length - 1] + '"]').parent().addClass('active');
        });

    });

    var close = document.getElementsByClassName("closebtn");
    var i;

    for (i = 0; i < close.length; i++) {
        close[i].onclick = function () {
            var div = this.parentElement;
            div.style.opacity = "0";
            setTimeout(function () {
                div.style.display = "none";
            }, 60);

        }
    }

</script>