-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 18, 2022 at 03:19 PM
-- Server version: 5.7.38-0ubuntu0.18.04.1
-- PHP Version: 7.3.33-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blogs`
--
CREATE DATABASE IF NOT EXISTS `blogs` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `blogs`;

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `description`, `image`, `date`, `user_id`) VALUES
(1, 'BEEF ENCHILADAS', 'One of my longtime favorite recipes to whip up for a big dinner party, wrap up in a casserole dish to bring to friends, or bake up on a regular old weeknight here with just the three of us â€” say hello to my favorite beef enchiladas! â™¡\r\n\r\nThese delicious Tex-Mex-inspired enchiladas are loaded up with with tender ground beef (or we often make ours nowadays with plant-based meat), green chiles, onion, garlic, beans, cheese, your choice of tortillas, and my favorite red enchilada sauce. And when baked to warm and melty perfection, then served up with a few slices of avocado and perhaps a sprinkling of fresh cilantro and crumbled cotija, I can tell you â€” after a decade plus of serving these enchiladas to loved ones â€” that they are always, always such a hit.\r\n\r\nTheyâ€™re perfect for prepping ahead of time, making this recipe a great option for easy entertaining or popping in the oven after a long day of work. And they can also easily be made gluten-free with corn tortillas and my homemade sauce, if youâ€™d like.\r\n\r\nIâ€™ve made this beef enchiladas recipe more times than I can count and know weâ€™ll certainly be enjoying it in our house for many years to come. So if you have yet to give it a try, itâ€™s time!\r\n\r\n\r\nBeef enchilada filling spread on a tortilla with cheese and red enchilada sauce\r\nBEEF ENCHILADA INGREDIENTS\r\nDetailed instructions for how to make beef enchiladas are included in the recipe box below. But before we get to the recipe, here are a few quick notes about the ingredients you will needâ€¦\r\n\r\nBeef: I typically use organic lean ground beef for this recipe. But if you happen to have leftover cooked diced or shredded beef on hand (such as shredded beef barbacoa), it will work well in this recipe too.\r\nOnion and garlic: We will sautÃ© these until softened to help season the filling.\r\nDiced green chiles: I typically toss a small can of diced green chiles into the filling. But you could sautÃ© a diced jalapeÃ±o with the onion if you would like to amp up the heat even more.\r\nBeans: I love adding beans to enchilada fillings for extra protein, and pinto beans or black beans are my preference with this recipe. But white beans, lentils or chickpeas would also work well here too.\r\nTortillas: Corn tortillas are traditionally used in Mexican-style enchiladas, but I usually use flour tortillas in this recipe (which are more commonly used in Tex-Mex and American-style enchiladas) because they are much easier to roll. That said, any type of tortillas that you prefer can work in this recipe.\r\nCheese: I most often use Pepper Jack or a Mexican-blend of shredded cheese for these enchiladas, but feel free to use whatever variety of cheese that you love best.\r\nCumin, sea salt and freshly-cracked black pepper: These will serve as our the basic seasonings for the beef filling, but the enchilada sauce will add much more flavor too.\r\nToppings: When it comes to enchiladas, I vote the more toppings the merrier! Any combination of chopped fresh cilantro, sliced avocado, diced red onion (or quick pickled red onions), sliced radishes, fresh or pickled jalapeÃ±os, sour cream or Mexican crema, and/or extra cheese would be delicious here.\r\nEnchilada sauce: Finally, you can use any type of enchilada sauce that you prefer for this recipe, but I highly recommend using my favorite homemade enchilada sauce. It only takes a few extra minutes to prep and can simmer on the stovetop while you prepare the rest of the enchiladas. Itâ€™s made with basic pantry ingredients and tastes a million times better than the store-bought canned stuff.', 'enchiladas-4-1220718023803.jpg', '2022-07-18 18:38:03', 1),
(2, 'ALIâ€™S GUIDE TO ISTANBUL', 'Is Istanbul on your travel bucket list yet?  If not â€” ohhh friends â€” it needs to be. â™¡\r\n\r\nWe booked our trip there this past fall on a whim, thanks to some crazy-cheap plane tickets that we happened to spot online.  But while Istanbul had always been on our â€œsomedayâ€ travel bucket list, neither Barclay or I admittedly knew all that much about the city or its culture or its food beforehand.  We happen to have some good friends here in Barcelona who are from Istanbul, though, who loaded us up with lots of great recommendations for places to see and things to do before we set off.  We also thankfully made the decision to pre-book more tours than usual, especially since we didnâ€™t speak the language and knew so little about Istanbulâ€™s history.  (Also because Barclay and I couldnâ€™t agree on which food tour to pick â€” whiiiich meant that we ended up booking two, which ended up being one of the most delicious decisions of our lives.)\r\n\r\nAnd wow, you guys, this city literally blew us away at every level.\r\n\r\nWe loved Istanbul! â™¡â™¡â™¡\r\n\r\nHistory-wise, the city was endlessly fascinating, with so much to study and so many great places to see.  Accessibility-wise, the neighborhoods that we wanted to visit all ended up being super-walkable and easy to navigate with trams and ferries, and we loved the cute, centrally-located little neighborhood (KarakÃ¶y) where we stayed.  Relaxation-wise, we splurged on a bucket list fancy hammam during our last morning there, and loved every single minute of the experience.  And food-wise, ohhhhh my goodness, itâ€™s no exaggeration to say that our five days in Istanbul were some of the most delicious days of my life.  Thanks to the two food tours and to Istanbulâ€™s meze (small plates) style of eating, we got to sample a record number of local dishes on this trip.  And I was in Turkish food heaven.\r\n\r\nAnyway, tons of you messaged on Instagram asking for our trip recs and for my thoughts on traveling to the city.  And I would just say â€” if Istanbul also happens to be on your travel bucket list, bump it up to the top and make your way there as soon as you can.  The city seems to be poised in that sweet spot right now where tourism is quickly developing, but it hasnâ€™t yet reached that saturation point where everything is overdeveloped and overpriced and overrun with visitors.  So this felt like a really special window of time to be there and see the city.  And if you are looking for some great recommendations, here are all of my faves for:', 'Istanbul-22220718023913.jpg', '2022-07-18 18:39:13', 1),
(3, 'A NOTE FROM ALI', 'Tap tap â€” is this thing on?\r\n\r\nHi, hello! Iâ€™m so sorry that I unexpectedly went MIA here these past few months. I had initially just planned to take the last few weeks of December off from posting while we were traveling back to the States to visit our family. But then one thing after another came up after we made it home â€” childcare starts and stops, relentless winter colds, website redesign deadlines â€” and anyway, I feel like I blinked and itâ€™s somehow already February!\r\n\r\nIâ€™m happy to say that tomorrow we will officially be getting back into the rhythm of sharing new recipes here again. (So many good ones ready for you this month!) But today, I wanted to pop in first to share a few personal updates here regarding the new year ahead.\r\n\r\nNamely, Iâ€™m finally taking the leap and making a bit of a career pivot. â™¡\r\n\r\nI originally began Gimme Some Oven back in 2009 because I believed â€” and still believe, now more than ever â€” in the role that really good food can play in bringing people together to connect around the table. But the longer I have worked â€œin food,â€ the more aware Iâ€™ve also become aware of what a privilege it is to have a table full of food each day when so many people do not (even though there is more than enough food grown to feed every person in the world). Issues surrounding access to food have always felt so complicated and overwhelming. But the more Iâ€™ve begun to study and learn about the specific systems that lead to food insecurity, the more Iâ€™ve realized itâ€™s possible to get involved and make a real difference. And the more Iâ€™ve gotten involved in a volunteer capacity, the more Iâ€™ve realized how much I really want this sort of work to be an even larger part of my life. Which â€” to be honest â€” has left me feeling conflicted because I love my full-time work job here and havenâ€™t wanted to completely give up blogging.\r\n\r\nWell to make a very long story short, the switch eventually flipped and I realized that I actually have the opportunity to do both. â™¡â™¡ So over the next few months, I am going to be shifting my work hours/responsibilities here to focus solely again on the original reasons I got into blogging (i.e. cooking up yummy new recipes, writing about them, and connecting with all of you), and bring on new help with photography and some of the other time-consuming techy parts of running a website. Then later this spring, Iâ€™m planning to officially begin working part-time with The Wash Project, helping out with communications and supporting local leaders in Mali with various food insecurity initiatives. (Some of which will be building upon the incredible community garden project that you all helped to fund this past year â€“ exciting!)\r\n\r\nAs for Gimme Some Oven, my hope is actually that very little will change here when it comes to recipe content. Iâ€™ll still be the one cooking and writing about new recipes each week, and weâ€™ve been working on a few upcoming projects that will hopefully make this site more helpful and organized than ever. Iâ€™m also hoping to have time to share my usual â€œCurrentlyâ€ updates a bit more often â€” which is actually what todayâ€™s post originally started out to be, but then this intro became so long that I decided to separate the fun updates about Teo and life here in Barcelona into another post. ;) So stay tuned for some cute 4-toothed baby smiles and pup-dates coming your way soon.\r\n\r\nAnyway, I hesitated to even really mention this change publicly, since I know that many of you are just here for the food. (Which is totally cool, recipes will be continuing!) But for those of you whoâ€™ve been with me a long time, I just wanted to share the latest about where this career in blogging â€” a job that wasnâ€™t even a â€œthingâ€ back when I first started this site as a hobby, but has been made possible over the years thanks to you all and your presence here â€” is now evolving. It feels like the other half of my heart for food is finally now going to have an official part of my work week. And Iâ€™m really excited about the potential for some great crossover ahead, especially sharing conversations and opportunities here for those who would like to get involved in some of these initiatives. More to come.\r\n\r\nPues, ya estÃ¡ â€” thatâ€™s the scoop! Thanks for taking the time to read this today, and as always, thank you so much for your continued support of this little corner of the internet. Iâ€™m more excited than ever for this next chapter, and look forward to sharing more updates and new recipes very soon.\r\n\r\n(Also, yes, that dreamy giant cinnamon roll recipe is also coming your way very soon.)\r\n\r\nLove,\r\nAli', 'Cinnamon-Roll-1220718024009.jpg', '2022-07-18 18:40:09', 1),
(4, 'WHAT TO EXPECT ON A VIKING OCEAN CRUISE', 'Alright guys, cozy up.  (Long post alert!)  Today Iâ€™m finally sharing all about our recent sail through the Baltic Sea with Viking Ocean Cruises!\n\nAs I mentioned in my post about spending three days beforehand in Stockholm, this trip was both Barclayâ€™s and my first time visiting each of the countries on this cruise â€” Sweden, Finland, Russia, Estonia and Poland.  So we were excited and curious to experience this part of Europe.  (Spoiler alert: we were surprised how much we loved it!)  And also, super curious to experience our first Viking ocean cruise.\n\nAs some of you might remember, we actually had the chance to sail through France on a Viking river cruise a few years ago, which we both really enjoyed.  But we were curious how the Viking vibe would translate to a much larger ocean cruise ship, and how the experience overall would differ.  Turns out, it was pretty different.  But also, incredibly fun.  We had a blast, and were hopelessly spoiled on board, and didnâ€™t want our week to end.  So to answer the question a hundred or so of you asked â€” yes, we would totally recommend it!\n\nThat said, the Viking ocean cruise experience was definitely apples-and-oranges different from our Viking river cruise experience.  So for any of you considering an upcoming cruise for yourself, I wanted to follow up my river cruise post with an ocean cruise post, and tell you all about it, and also offer a few thoughts about how to choose which kind of cruise might be best for you.\n\nSo hereâ€™s the scoop on everything about our Viking ocean cruise, including details on:\n\nThe Ship (staterooms, restaurants, spa, entertainment, gathering spaces, and more)\nThe Cruise (our itinerary, tours, things to do, destination travel recs)\nThe Food (this one gets its own category, naturally)\nOur Best Tips (a few things weâ€™ve learned)\nOcean Cruise vs. River Cruise (some closing thoughts)\n\n\nOur cruise ship: The Viking Star\n\nTHE SHIP\nAlright, letâ€™s chat first about our home at sea during the week â€” the Viking Star.\n\nAs you can see, this is not one of the small river boats that you saw advertised on Downton Abbey commercials years ago.  This is a legit cruise ship.  9 floors, 465 staterooms, 4 full restaurants, 3 pools, a gorgeous spa, endless comfy deck chairs and beautiful lounges to hang out in, and about a million things to do.\n\nBasically, we learned that on a Viking ocean cruise that the ship is totally a destination in and of itself.\n\nThere are so many things that you can choose to take part in each day, so many restaurants and bars and food you can sample, so many places to relax and hang out, and so many amazing staff members working hard to make everything run smoothly.  You have the option of trying to squeeze as many things as you can in a day on the ship.  Or hey â€” if you want to just kick it for a few hours and soak up some introverted-time-with-a-view from your roomâ€™s private balcony â€” room service is also completely included all day long.  Basically, the ship is the epitome of â€œyouâ€™ve got options.â€  ;)\n\nBarclay and I especially loved our specific ship because it was:\n\nBeautiful.  Think bright, clean, modern, Scandinavian design everywhere you turn.  And windows everywhere. Basically, a Vitamin D loverâ€™s dream come true.  :)\nNot crowded.  One of the things we love about Viking cruise ships is that they only bring on board about half the number of passengers as most other cruise ships their size.  Which means that you get all of the amenities or a large ship, but itâ€™s not crowded at all.  Truly, we couldnâ€™t believe how easy it was to find a deck chair, walk up to a buffet, find an open hot tub, and attend any activity we wanted all week long.  In our experience, lines were almost non-existent.\nEfficient.  Speaking of lines, as someone who nerds out on efficiency, I was also blown away by Vikingâ€™s ability to move 1000+ passengers around so quickly and smoothly.  Our onboarding check-in when we arrived literally took less than 5 minutes (no lines!), the initial safety presentation was quick and organized, and all of our excursions were prompt and easy to navigate.\nTranquil.  Similar to our experience on the Viking river cruise, we were also impressed with how quiet and tranquil the ship was in general.  Especially after busy days in port, it was always nice to come back to such a relaxing environment.\nNerdy.  By contrast to some of the casinos and clubs on other cruise ships, we both loved that this ship was proudly nerdy.  From PhD guest lecturers on board each day, to an extensive ship library, to TEDTalks being streamed daily, to the most organized jigsaw puzzle station weâ€™ve ever seen â€” the ship is designed for people who really love to learn.  We dug it. :)\nAll-inclusive.  This was a big one.  By contrast to some other â€œall-inclusiveâ€ ships and hotels, Viking prides itself on not nickel-and-diming you every step along the way.  Case in point?  Daily shore excursions, 24-hour room service, unlimited wifi, daily spa access (with the most amazing hot tubs!), beer/wine/soft drinks at every meal, specialty teas and coffees, port taxes and fees, laundry machines, and ground transfers with air purchase are all included.  Amazing.', 'Viking-Ocean-Cruise-1 (1)220718024133.jpg', '2022-07-18 18:52:34', 1),
(5, 'TACO SEASONING', 'Getting ready to make a recipe that calls for taco seasoning, but donâ€™t have a jar on hand?\r\n\r\nNo worries â€” just mix up a quick batch of this homemade taco seasoning! â™¡\r\n\r\nAll you need is a handful of spices that you likely already have in your pantry. Simply whisk them all together, store in a sealed spice jar, and use whenever you are ready!\r\n\r\nThis homemade taco seasoning is â€” of course â€” fantastic when used to season the meat, seafood, veggies or beans in your favorite taco recipe. But donâ€™t forget that it can also be used a million other ways as well! Iâ€™m especially partial to using taco seasoning to flavor Mexican rice or a side of black beans. It works great as a rub for steak, chicken, fish or shrimp, especially during summertime grilling season. I also often sprinkle it in soups or add it to a vinaigrette when Iâ€™m craving a zesty salad. And if youâ€™re feeling adventurous â€” trust me on this one â€” itâ€™s actually surprisingly delicious when sprinkled on popcorn too!\r\n\r\nHowever you use it, this homemade taco seasoning recipe is a great one to have in your back pocket. So go raid your spice drawer, and letâ€™s make a quick batch together!\r\n\r\n\r\nHomemade Taco Seasoning IngredientsHOMEMADE TACO SEASONING INGREDIENTS:\r\nTo make this homemade taco seasoning recipe, you will need the following spices:\r\n\r\nChili powder: Please note that this is American-style chili powder, which includes a mix of spices. (International readers, it is fairly mild and not cayenne. â™¡)\r\nGround cumin, garlic powder, onion powder and dried oregano: Pretty straight-forward.\r\nPaprika: I prefer to use smoked paprika, but you could also use standard or sweet paprika if you prefer.\r\nSalt and black pepper: I used fine sea salt and finely-ground black pepper for this recipe.\r\nCrushed red pepper flakes: To add a bit of heat. If you prefer a milder taco seasoning, you can leave these out. Or if you donâ€™t have crushed red pepper flakes on hand, you could also sub in a pinch of cayenne.\r\n \r\n\r\nHow To Make Taco Seasoning\r\nHOW TO MAKE TACO SEASONING:\r\nTo make homemade taco seasoning, simplyâ€¦\r\n\r\nCombine the ingredients.  Combine everything together in a small jar, cover and shake until evenly combined.\r\nStore. A batch of homemade taco seasoning should last you about a year!', 'Taco-Seasoning-Recipe-4220718024253.jpg', '2022-07-18 18:42:53', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `Name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `Name`) VALUES
(1, 'test', '098f6bcd4621d373cade4e832627b4f6', 'test');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
