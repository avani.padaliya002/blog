<?php
session_start();
include('config.php');
if (isset($_SESSION['id']) && isset($_SESSION['username'])) {

    $query = "select * from blogs where id='" . $_GET['id'] . "'";
    $exeQuery = mysqli_query($conn, $query);
    if (mysqli_num_rows($exeQuery)) {
        $blogs = mysqli_fetch_all($exeQuery, MYSQLI_ASSOC);
    }
    ?>
    <html>
    <head>
        <title>Blogs</title>
        <link rel="stylesheet" type="text/css" href="style.css">

    </head>
    <body>
    <div class="header">
        <div class="menu-bar">
            <nav>
                <ul class="menu">
                    <li class=''><a href="home.php">Home</a></li>
                    <li class=''><a href="blogs.php">Blogs</a></li>
                    <li class=''><a href="logout.php">Logout</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="main">
        <div class="container">
            <div class="blog">

                <?php
                foreach ($blogs as $blog) { ?>

                    <div style="text-align:justify; box-shadow: 0 0 20px 16px #0000001f; padding: 10px;margin: 10px; display: block; margin-left: auto; margin-right: auto; width: 90%;">
                        <div style="padding: 25px;">
                            <img src="/blog/images/<?= $blog['image'] ?>"
                                 style="height:auto; display: block; margin-left: auto;margin-right: auto;width: 50%;">
                            <h3 style="text-align:center"><?= $blog['title']; ?></h3>
                            <h5 style="text-align: center"><?php $date = date_create($blog['date']);
                                echo date_format($date, 'Y-M-d'); ?></h5>
                            <section>
                                <p style="padding-left: 190px;padding-right: 190px;"> <?php echo $blog['description']; ?></p>
                            </section>
                        </div>
                    </div>

                <?php } ?>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="menu-bar">
            <nav>
                <footer class="footer">
                    <h6>All right reserved</h6>
                </footer>
            </nav>
        </div>
    </div>
    </body>
    </html>

    <?php
} else {
    header("Location:login.php");
    exit();
} ?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">

    function deleteBlog(data) {
        if (confirm("Delete Account?")) {
            $.ajax({
                type: "POST",
                url: "deleteBlog.php",
                data: {"id": data},
                dataType: "json",
                success: function (data) {
                    window.location.href = "blogs.php";
                },
                error: function (request, error) {
                    alert("Request: " + JSON.stringify(request));
                }
            });
        }
    }

    $("document").ready(function () {
        $(function () {
            $('.menu a[href="' + location.pathname.split("/")[location.pathname.split("/").length - 1] + '"]').parent().addClass('active');
        });

    });


    var close = document.getElementsByClassName("closebtn");
    var i;

    for (i = 0; i < close.length; i++) {
        close[i].onclick = function () {
            var div = this.parentElement;
            div.style.opacity = "0";
            setTimeout(function () {
                div.style.display = "none";
            }, 60);

        }
    }

</script>