<?php
session_start();
include('config.php');

if (isset($_SESSION['id']) && isset($_SESSION['username'])) {

    if (isset($_GET['id'])) {
        $query = "select * from blogs where id ='" . $_GET['id'] . "'";
        $exeQuery = mysqli_query($conn, $query);
        if (mysqli_num_rows($exeQuery)) {
            $blogs = mysqli_fetch_assoc($exeQuery);
        }
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        $target_dir = "images/";
        $imageName = explode('.', basename($_FILES["fileToUpload"]["name"]));
        $target_file = $target_dir . $imageName[0] . date('ymdhis') . '.' . $imageName[1];
        $image = $imageName[0] . date('ymdhis') . '.' . $imageName[1];

        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

        if (!empty($_POST['id'])) {

            $query = "select * from blogs where id ='" . $_POST['id'] . "'";
            $exeQuery = mysqli_query($conn, $query);

            if (mysqli_num_rows($exeQuery)) {

                $blogs = mysqli_fetch_assoc($exeQuery);
            }
            if (!empty($_FILES['fileToUpload']['name'])) {

                if (!empty($_POST['title']) && !empty($_POST['desc']) && !empty($_FILES['fileToUpload']['name'])) {

                    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                        && $imageFileType != "gif") {

                        $_SESSION['error'] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                        $uploadOk = 0;
                        header("Location: blogs.php");
                        exit();
                    }
                    // Check if file already exists
                    if (file_exists($target_file)) {
                        $_SESSION['error'] = "Sorry, file already exists.";
                        header("Location: blogs.php");
                        exit();
                        $uploadOk = 0;
                    }

                    if ($uploadOk == 0) {
                        $_SESSION['error'] = "Sorry, your file was not uploaded.";
                        header("Location: blogs.php");
                        exit();
// if everything is ok, try to upload file
                    } else {

                        $title = validate($_POST['title']);
                        $desc = validate($_POST['desc']);


                        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                        } else {
                            $_SESSION['error'] = "Sorry, there was an error uploading your file.";
                            header("Location: blogs.php");
                            exit();
                        }

                        $sqlupdateImage = "update blogs set title='" . $title . "',description='" . $desc . "', image='" . $image . "',user_id='" . $_SESSION['id'] . "' where id='" . $_POST['id'] . "'";
                        $sqlUpdateImageResult = mysqli_query($conn, $sqlupdateImage);
                        if ($sqlUpdateImageResult) {
                            unlink(__DIR__ . '/images/' . $blogs['image']);
                            $_SESSION['success'] = "Blog updated Successfully";
                            header("Location: blogs.php");
                            exit();
                        }
                    }
                } else {
                    $_SESSION['error'] = mysqli_error($conn);
                    header("Location: blogs.php");
                    exit();
                }

            } else {

                $title = validate($_POST['title']);
                $desc = validate($_POST['desc']);

                $sqlupdate = "update blogs set title='" . $title . "',description='" . $desc . "',user_id='" . $_SESSION['id'] . "' where id='" . $_POST['id'] . "'";
                $sqlUpdateResult = mysqli_query($conn, $sqlupdate);
                if ($sqlUpdateResult) {
                    $success = 'Blog Updated Successfully';
                    $_SESSION['success'] = $success;
                    header("Location: blogs.php");
                    exit();
                } else {
                    $_SESSION['error'] = mysqli_error($conn);
                    header("Location: blogs.php");
                    exit();
                }
            }
        } else {

            if (!empty($_POST['title']) && !empty($_POST['desc']) && !empty($_FILES['fileToUpload']['name'])) {

                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                    $_SESSION['error'] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                    $uploadOk = 0;
                    header("Location: blogs.php");
                    exit();
                }
                // Check if file already exists
                if (file_exists($target_file)) {
                    $_SESSION['error'] = "Sorry, file already exists.";
                    header("Location: blogs.php");
                    exit();
                    $uploadOk = 0;
                }

                if ($uploadOk == 0) {
                    $_SESSION['error'] = "Sorry, there was an error uploading your file.";
                    header("Location: createBlog.php");
                    exit();
// if everything is ok, try to upload file
                } else {

                    $title = validate($_POST['title']);
                    $desc = validate($_POST['desc']);

                    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                    } else {
                        $error = "Sorry, there was an error uploading your file.";
                    }
                    $sql2 = "INSERT INTO blogs(title, description, image,user_id) VALUES('" . $title . "', '" . $desc . "', '" . $image . "','" . $_SESSION['id'] . "')";
                    $result2 = mysqli_query($conn, $sql2);
                    if ($result2) {
                        unlink(__DIR__ . '/images/' . $blogs['image']);
                        $_SESSION['success'] = "Blog created Successfully";
                        header("Location: blogs.php");
                        exit();
                    } else {
                        $_SESSION['error'] = mysqli_error($conn);
                        header("Location: createBlog.php");
                        exit();
                    }
                }
            } else {
                $_SESSION['error'] = 'All fields are required';
                header("Location: createBlog.php");
                exit();

            }
        }
    }

    ?>
    <html>
    <head>
        <title>Create Blog</title>
        <link rel="stylesheet" type="text/css" href="style.css">

    </head>
    <body>
    <div class="header">
        <div class="menu-bar">
            <nav>
                <ul class="menu">
                    <li class=''><a href="home.php">Home</a></li>
                    <li class=''><a href="blogs.php">Blog</a></li>
                    <li class=''><a href="logout.php">Logout</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="main">
        <form method="post" action="createBlog.php" enctype="multipart/form-data">

            <?php if (isset($_SESSION['error'])) { ?>
                <div class="alert">
                    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                    <?php echo $_SESSION['error'];
                    unset($_SESSION['error']); ?>

                </div>
            <?php } ?>

            <?php if (isset($_SESSION['success'])) { ?>
                <div class="alert success">
                    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                    <?php echo $_SESSION['success'];
                    unset($_SESSION['success']); ?>
                </div>
            <?php } ?>
            <input type="hidden" name="id" value="<?= $blogs['id'] ?>">

            <label>Title</label>
            <input type="text" name="title" placeholder="Enter Title"
                   value="<?php echo (isset($blogs['title'])) ? $blogs['title'] : ''; ?>">

            <label>Description</label>
            <textarea name="desc"
                      placeholder="Enter Description"><?php echo (isset($blogs['description'])) ? $blogs['description'] : ''; ?></textarea>

            <label>Image</label>
            <div class="empty-text">
                <?php if (isset($blogs['image'])) { ?> <img src="/blog/images/<?= $blogs['image'] ?>"
                                                            style="height: 180px; width:200px;"><?php } ?>
            </div>
            <input type="file" name="fileToUpload" id="fileToUpload"
                   value="<?php echo (isset($blogs['image'])) ? $blogs['image'] : ''; ?>">

            <button type="submit" name="submit"><?php echo (isset($blogs['id'])) ? 'Update' : 'Create'; ?></button>

        </form>
    </div>
    </body>
    </html>

    <?php
} else {
    header("Location:login.php");
    exit();
} ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>

    $('#fileToUpload').on('change', function () {

        var file = this.files[0];
        var imagefile = file.type;
        var imageTypes = ["image/jpeg", "image/png", "image/jpg", "image/gif"];
        if (imageTypes.indexOf(imagefile) == -1) {
            //display error
            return false;
            $(this).empty();
        } else {
            var reader = new FileReader();
            reader.onload = function (e) {
                $(".empty-text").html('<img src="' + e.target.result + '" style="height: 180px; width:200px;" />');
            };
            reader.readAsDataURL(this.files[0]);
        }

    });

    var close = document.getElementsByClassName("closebtn");
    var i;

    for (i = 0; i < close.length; i++) {
        close[i].onclick = function () {
            var div = this.parentElement;
            div.style.opacity = "0";
            setTimeout(function () {
                div.style.display = "none";
            }, 60);

        }
    }
</script>
