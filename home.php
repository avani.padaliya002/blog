<?php
session_start();
include('config.php');
if (isset($_SESSION['id']) && isset($_SESSION['username'])) {

    ?>
    <html>
    <head>
        <title>Home</title>
        <link rel="stylesheet" type="text/css" href="style.css">

    </head>
    <body>
    <div class="header">
        <div class="menu-bar">
            <nav>
                <ul class="menu">
                    <li class=''><a href="home.php">Home</a></li>
                    <li class=''><a href="blogs.php">Blogs</a></li>
                    <li class=''><a href="logout.php">Logout</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="main">
        <div class="blog-grid">
           <h1 style="text-align: center">Welcome!</h1>
        </div>
    </div>
    <div class="footer">
        <div class="menu-bar">
            <nav>
                <footer class="footer">
                    All right reserved
                </footer>
            </nav>
        </div>
    </div>

    </body>
    </html>

    <?php
} else {
    header("Location:login.php");
    exit();
} ?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">

    $("document").ready(function () {
        $(function () {
            $('.menu a[href="' + location.pathname.split("/")[location.pathname.split("/").length - 1] + '"]').parent().addClass('active');
        });

    });
</script>