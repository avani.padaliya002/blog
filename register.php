<?php
session_start();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    include('config.php');
    if (isset($_POST['uname']) && isset($_POST['password'])
        && isset($_POST['name']) && isset($_POST['re_password'])) {

        $uname = validate($_POST['uname']);
        $pass = validate($_POST['password']);

        $re_pass = validate($_POST['re_password']);
        $name = validate($_POST['name']);

        if (empty($uname)) {
            $error = "User Name is required";

        } else if (empty($pass)) {
            $error = "Password is required";

        } else if (empty($re_pass)) {
            $error = "Re Password is required";

        } else if (empty($name)) {
            $error = "Name is required";

        } else if ($pass !== $re_pass) {
            $error = "The confirmation password  does not match";

        } else {

            // hashing the password
            $pass = md5($pass);

            $sql = "SELECT * FROM users WHERE username='$uname' ";
            $result = mysqli_query($conn, $sql);

            if (mysqli_num_rows($result) > 0) {
                $error = "The username is taken try another";

            } else {
                $sql2 = "INSERT INTO users(username, password, name) VALUES('$uname', '$pass', '$name')";
                $result2 = mysqli_query($conn, $sql2);
                if ($result2) {
                    $success = "Your account has been created successfully";

                } else {
                    $error = "unknown error occurred";

                }
            }
        }

    } else {
        header("Location:register.php");
        exit();
    }
}
?>


<!DOCTYPE html>
<html>
<head>
    <title>SIGN UP</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<form action="register.php" method="post">
    <h1>Sign Up</h1>
    <?php if (isset($error)) { ?>
        <p class="error"><?= $error ?> </p>
    <?php } ?>

    <?php if (isset($success)) { ?>
        <p class="success"><?php echo $success; ?></p>
    <?php } ?>

    <label>Name</label>
    <input type="text" name="name" placeholder="Enter Name">

    <label>User Name</label>
    <input type="text" name="uname" placeholder="Enter User-Name">
    <label>Password</label>
    <input type="password" name="password" placeholder="Enter Password">

    <label>Re Password</label>
    <input type="password" name="re_password" placeholder="Re-Enter Password">

    <button type="submit">Sign Up</button>
    <a href="login.php">Already have an account?</a>
</form>
</body>
</html>
